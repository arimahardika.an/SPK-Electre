/*
* Author : Ari Mahardika
* Email  : arimahardika.an@gmail.com
* Git    : https://gitlab.com/u/Ari.Mahardika
*
* Date   : 20/12/2-16
* Description : Electre
* Copyright 2016
*/
#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;

int main(){
	
	int alternatif, kriteria, bobot;
	
	cout<<"masukkan jumlah alternatif : ";
	cin>>alternatif;
	cout<<"masukkan jumlah kriteria : ";
	cin>>kriteria;
	cout<<"masukkan jumlah bobot : ";
	cin>>bobot;
	
	int matrix_keputusan_raw[alternatif][kriteria];
	int matrix_bobot[bobot];
	
	//input nilai matrix
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			cout<<"["<<i<<"]["<<j<<"] : ";
			cin>>matrix_keputusan_raw[i][j];
		}
	}
	
	//menampilkan matrix
	cout<<endl<<"Matrik keputusan"<<endl;
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			cout<<setw(5)<<matrix_keputusan_raw[i][j]<<" ";
		}
		cout<<endl;
	}
	cout<<endl;
	
	//input nilai bobot
	for(int i=0;i<bobot;i++){
		cout<<"matrix_bobot["<<i<<"] : ";
		cin>>matrix_bobot[i];
	}
	cout<<endl;
	
	//menampilkan bobot
	cout<<"Matrik bobot"<<endl;
	for(int i=0;i<bobot;i++){
		cout<<setw(5)<<matrix_bobot[i]<<" ";
	}
	cout<<endl;
	
//	melakukan normalisasi
//	tahapan :
//		dikuadratkans
//		dijumlahkan
//		diakar
//		x dibagi hasil diatas

	int matrix_dikuadratkan[alternatif][kriteria];
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			matrix_dikuadratkan[i][j] = pow(matrix_keputusan_raw[i][j],2);
		}
	}
	
//	cout<<endl<<"Matrik dikuadratkan"<<endl;
//	for(int i=0;i<alternatif;i++){
//		for(int j=0;j<kriteria;j++){
//			cout<<setw(5)<<matrix_dikuadratkan[i][j]<<" ";
//		}
//		cout<<endl;
//	}
//	cout<<endl;
	
	int matrix_kuadrat_dijumlahkan[kriteria]={0,0,0};
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			matrix_kuadrat_dijumlahkan[i] += matrix_dikuadratkan[j][i];
		}
	}
	
//	cout<<endl<<"Matrik dikuadratkan + dijumlahkan"<<endl;
//	for(int i=0;i<alternatif;i++){
//		cout<<setw(5)<<matrix_kuadrat_dijumlahkan[i]<<" ";
//	}
//	cout<<endl;
	
	float matrix_diakar[kriteria];
	for(int i=0;i<alternatif;i++){
		matrix_diakar[i] = pow(matrix_kuadrat_dijumlahkan[i],0.5);
	}
	
//	cout<<endl<<"Matrik (dikuadratkan + dijumlahkan) kemudian diakar"<<endl;
//	for(int i=0;i<alternatif;i++){
//		cout<<setw(5)<<matrix_diakar[i]<<" ";
//	}
//	cout<<endl;
	
	float matrix_normalisasi[alternatif][kriteria];
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			matrix_normalisasi[j][i] = matrix_keputusan_raw[i][j]/matrix_diakar[j];
		}
	}
	
	cout<<endl<<"Matrik normalisasi"<<endl;
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			cout<<matrix_normalisasi[j][i]<<" ";
		}
		cout<<endl;
	}
	cout<<endl;
	
	float pembobotan[alternatif][kriteria];
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			pembobotan[i][j] = matrix_normalisasi[i][j] * matrix_bobot[i];
		}
	}
	
	cout<<endl<<"Matrik pembobotan"<<endl;
	for(int i=0;i<alternatif;i++){
		for(int j=0;j<kriteria;j++){
			cout<<pembobotan[j][i]<<" ";
		}
		cout<<endl;
	}
	cout<<endl;
	
return 0;
}

